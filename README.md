# The apps

## Start of the PFE

* The system starts with a combination of "systemv" (/etc/init.d) and "upstart" scripts (/etc/event.d). Systemv launches for exemple ReadIVM, StoreEquipmentsInformations and TecSystem.

* PFE applications are started (or stopped) thanks to `/etc/init.d/TecSystem start` (or stop) which starts the "TerminalManager" application. This application manages the launch of other applications (Computer/Bus/Ticket/HMI/Market Order).
The application settings are configured in the scripts /etc/event.d/.

* The shutdown is detected by the "TerminalManager" application (the "TecSystem" service must therefore have
has been launched for the detection of the stop to be active).

* Note: the /etc/rc.sysinit script (packaged with the kernel) contains some changes, including
for date management. You should also keep an eye on this script. This is one of the very first scripts to
start.


## TecSystem


As explained previously, the apps are launched by the following command line `/etc/init.d/TecSystem start`

This script prepares the PFE and its ports (Serial ports, Can interface, Message queues). And then, it launches the TerminalManager, redirecting the output to /dev/null. 


## Terminal Manager

launches ApplicationManager StartInMode `commandLine = "/sbin/initctl emit startModeBus";`


## List of all apps

All the following apps are built by the ./build_all.sh script:

* AddAlerteOperationnelle/AddAlerteOperationnelle
* AffInterieurs/AffInterieurs
* AffVideo/AffVideoUnitTests
* BadgeReaderConfigure/BadgeReaderConfigure
* Billetique/Billetique
* CanData/CanData
* CanData/CanSimulator
* CommonLibs/gps_nmea_reader
* CommonLibs/tfifo/Tests/test_tfifo
* CommonLibs/tfsm/Tests/basic_test/basic_test
* CommonLibs/tfsm/Tests/nested_fsm_test/nested_fsm_test
* commons/testsuite/testsuite_cmdline
* DataTransfer/bin/data_transfer
* DataTransfer/bin/sgip_transfer
* Diagnostic/DiagnosticKeyboard/src/diagnostic_keyboard
* Diagnostic/Keyboard/src/keyboard
* Diagnostic/Maintenance/src/maintenance
* DriverPrinter/testPrinter/testPrinter
* Eis771/BilletiqueSimu/BilletiqueSimu : TCP UDP connection simulator (with validators)
* Eis771/ProApi/ProApi : virtual PFE emulating Ticketing behaviour 
* Eis771/Protocole/UnitTest/eis771_protocole_unittest
* Eis771/ValideurSimu/ValideurSimu : validator simulator
* Girouettes/Girouettes
* GsmVoiceInhibition/GsmVoiceInhibition
* hacheurAlstom/fakeServer
* hacheurAlstom/hacheurAlstom
* hacheurAlstom/relayServer
* hmi/src/hmi
* hmi/src/messagedisplay
* hmi/src/spinner
* hmiPressKeySimulator/hmiPressKeySimulator
* htpdate/htpdate
* I2CServer/I2CServer
* LogAndResetGsmData/LogAndResetGsmData
* Ordibus/Ordibus
* OrdreDeMarche/OrdreDeMarche
* OrdreDeMarcheVersion/odmv_client
* OrdreDeMarcheVersion/odmv_server
* ReadIVM/ReadIvm
* SetDateTime/src/SetDateTime
* sgip/compare_planned_measured
* sgip/hastus_correct_lines_tec4
* sgip/hastus_lines2header
* sgip/hastus_make_map
* sgip/hastus_stops2header
* sgip/hastus_stops_sap2header
* sgip/odm_analysis_format_log
* sgip/pfelog2sgip
* sgip/sgip2atp
* sgip/sgip2atp_patch1
* sgip/sgip2bo
* sgip/sgip2eis772
* sgip/sgip2geoloc
* sgip/sgip2km
* sgip/sgip2sapstoptimes
* sgip/sgip2stopslist
* sgip/sgip2trips
* sgip/sgip2trx
* sgip/sgip_basic_analyzer
* sgip/sgip_controller
* sgip/sgip_decoder
* sgip/sgip_events_basics
* sgip/sgip_generator
* sgip/sgip_monitor
* sgip/sgip_simulator
* sgip/stop_controller
* sgip/transceiver_light_test
* StoreEquipmentsInformations/StoreEquipmentsInformations
* tecbadgereader/tecbadgereader
* TecWatchdog/watchdog
* TecWatchdog/watchdog_test
* tec-wlan-config/src/WlanManager
* TerminalManager/PrepareMessageQueues
* TerminalManager/TerminalManager
* Tests/TU/TU_BILLETIQUE/AffClient/AffCliTest
* Tests/TU/TU_BILLETIQUE/Valideurs/ValideursTest
* Tests/TU/TU_BUZZER/BuzzerTest
* Tests/TU/TU_DB/DBHealthCheck/DBodbcTest
* Tests/TU/TU_FILESYSTEM/filecontents
* Tests/TU/TU_FILESYSTEM/middleFile
* Tests/TU/TU_FILESYSTEM/test_mpio
* Tests/TU/TU_GIROUETTES/Hanover/HanoverTest
* Tests/TU/TU_GIROUETTES/Ibis/IbisTest
* Tests/TU/TU_GIROUETTES/Mitron/MitronTest
* Tests/TU/TU_GPSD/GpsdTest
* Tests/TU/TU_GSM/GsmTest
* Tests/TU/TU_GSM/GsmVoiceTest
* Tests/TU/TU_GSMDATA/gprsReset
* Tests/TU/TU_SAETEC7/SaeTec7Test
* Tests/TU/TU_SOCKET/BindTest
* Tests/TU/TU_SON/TU_SON
* UploadKontronLogToDb/UploadKontronLogToDb
* Utils/calibtouch/calibtouch
* Utils/mq_emulator/mq_emulator
* Utils/mq_emulator/update_odm_time
* Utils/reqtool/reqtool
* Utils/srwt_time/srwt_time

## CanData

CanData reads data sent on the can port and send it to other applications such as Billetique.
The data it reads can be for exemple the mileage, bus' fuel consumption...

CanData doesn't log or print anything yet but starting by the 19.11 release logs will be available.

### How to run

On the PFEv1, CanData is a startup service which is launched automatically.
It will start on a startModeBus event and stop on a shutdownSystem or runlevel event (when the system level is not 2,3,4 or 5).
It is also possible to start/stop it manually doing `start CanData` or `stop CanData`.

Since it is a service, you can find it in ___/etc/event.d/CanData.event___.
Its configuration will be placed in ___/etc/sysconfig/CanData___.

__How to test__

To test CanData, you need to put the CanData.event in ___/etc/event.d/___.
Then, you need to put the configuration file CanData in ___/etc/sysconfig/___.
An exemple of this configuration file is available in ___embedded_pfe/CanData/CanData.sysconfig___.

If you want to test it using a virtual can port, you need to replace `can0` by `vcan0` in the config file.

Then, you can send data on the can port which will then be evaluated by CanData.
To send data manually on the can port, you can use the __cansend__ tool for exemple `cansend vcan0 80FEC1EE#02080426A10D112A`.
Or you can use CanSimulator.

### Error codes/Results

CanData doesn't log or print anything yet but starting by the 19.11 release logs will be available.
It reads data on the canport, selects it and send it to other applications.

No error code is returned either.

Starting 19.11, some logs will ve available in _/var/log/messages_ and through BusExport.
Every minute, CanData will log a message gathering information such as the protocole version, the mileage, the consumption.

### Dependencies

The build of this app depends on :
* The sources from CanData
* Timer
* For linked librairies : TecLog, Boost, rt, pthread

### Note

Though a lot of information can be send by the can protocol, the CanData service only uses :
* Fuel Consumption: LFC (19.10 release)
* FMS-standard Interface Identity / Capabilities: FMS 12 (19.10)
* High Resolution Vehicle Distance: VDHR 14 (19.10)
* FMS Tell Tale Status: FMS1 24 (19.11 release)

TecSystem checks CanData's status and restarts it in case of problem.
It is allegedly possible to give a parameter to CanData at startup so that the app becomes CanLogger.

## CanSimulator

CanSimulator automates the process of sending data on the can port using a file gathering can data as a parameter.
This app can be used to test CanData by reproducing sending a specific series of information on the can port.

### How to run

Below is an exemple showing how to use CanSimulator :
```bash
CanSimulator --i vcan0 --f /path/to/SimulationData.can --t 100
# --i specifies the interface
# --f specifies the file containing can data
# --t specifies the delay between two frame sending
```

An exemple of the SimulationData.can can be found on the ___Y server___ in ___Y:\pfe\referentiel\pfe\canbus\simulation\___.
```bash
open 0 'can0'.
using interface name 'can0'.
new index 0 (can0)
can0 18FEC101 [8] 89 0A 4A 02 FF FF FF FF
can0 18FEE901 [8] FF FF FF FF D1 FB 02 00
```
Useful frames can be added to this frame, but it needs to respect the same format.

### Error codes/Results

When starting CanSimulator, a log is expected in _/var/log/messages_ confirming the launch of the application.
Then, each frame the simulator will send on the can port will also be printed in your terminal.

No error codes are given.

### Dependencies

The build of this app depends on :
* The sources from CanData
* Timer
* For linked librairies : TecLog, Boost, rt, pthread

### Note

To understand the data in the SimulationData file, you might need to download a datasheet for canbus protocol from the internet.


## DataTransfer

### Current state
### How to run
### Error codes/Results
### Dependencies
### Note

## BilletiqueSimu

BilletiqueSimu is a TCP UDP connection simulator for validators.
It connects to the validators (also known as TIEs), and enables you to manipulate them as you wish. You can reboot them, block them, gather infos on them, etc...

### How to run

On a working PFE,
1. Run `/etc/init.d/TecSystem stop` to free all process using the TIE or the sockets.
2. Run `./BilletiqueSimu 6001 6002` to launch the app. The parameters are accordingly the TCP port and the UDP port on which sockets will be created and TIEs will try to connect to.
3. Enter commands in your terminal such as displayed by the help menu.

### Results

When the app is launched, it should print the help menu listing all functions and how to execute them. 
If TIEs are connected to your machine, it should also display basic infos on them such as their IDs.
If TIEs aren't connected to your machine, the help menu should still be printed but when you type a command nothing will be printed and no error code will be returned.

### Dependencies

The build of this app depends on :
* The sources from interface and protocole of EIS771 (ie the validators' protocol)
* Boost : it's everywhere in the code (getting rid of it would essentially mean redoing it all)
* CommonDefs : limited to some shared #DEFINE
* CommonLibs : depends on timers, threads and messagequeue
* For linked librairies : TecLog (from CommonLibs), Boost, rt, pthread

### Note

An update of this application will be available with the 19.11 release. This new version will be named ValidatorsMon (for validators monitoring).
Thanks to the update, the use should be clearer and new functions available.

uniqueID is the TCP session number of the validator

## hmi

hmi is the interface displayed to the driver.
Thanks to this interface, the driver can sell tickets to the customers, check the progress of his trip, send messages to the dispatchers and more.

### How to run

1. You need to have display available on your machine.
2. Run `./hmi` to launch the app and the window should open.

### Results

When the app is launched, it should display the hmi window composed of six tabs named :
* Afficheurs : manages the outdoor displays
* Billettique : manages sales
* Ordibus : manages the communication
* Pilot. Auto. : manages the trip tracking (stops, lines)
* Maintenance : gives various infos for the upkeep
By default, the focus should be on 'Billettique' when the hmi first opens.

__TODO: insert caption here__

Warnings are expected when executing hmi, an exemple is given below :
```bash
(hmi_light:4204): GLib-GObject-WARNING **: invalid cast from 'GtkTextView' to 'GtkMisc'
(hmi_light:4204): Gtk-CRITICAL **: gtk_misc_set_alignment: assertion 'GTK_IS_MISC (misc)' failed
(hmi_light:4204): Gtk-CRITICAL **: gtk_widget_set_events: assertion '!GTK_WIDGET_REALIZED (widget)' failed
```

### Dependencies

The build of this app depends on :
* Webkit-1.0 (see notes below)
* libsoup
* hmi sources from the hmi/src directory
* CommonLibs
* CommonDefs
* OrdreDeMarche
* PaStates, Timer
* linked librairies : PlayWavFile, TecIni, TecSocket, TecLog, TecDBodbc, odbc, timers, Webkit, Gtk2, Alsa, rt, pthread

### Note

Another version without most dependencies exists and is named hmi_light.

We can get rid of the Webkit dependency. It was only used in one particular case when connection to Tie was lost and hasn't been used in 5 years.


## hmi_light

hmi is the light version of hmi.
It is an interface displayed for the driver.
Since this is the light version, it is not connected to any other service. Hence, its only purpose is to display a screen, the buttons aren't linked to callbacks.

### How to run

1. You need to have display available on your machine.
2. Run `./hmi_light` to launch the app and the window should open.

### Results

When the app is launched, it should display the hmi window composed of six tabs named :
* Billettique : manages sales
* Ordibus : manages the communication
* TelB : manages the outdoor displays
* Pilot. Auto. : manages the trip tracking (stops, lines)
* Maintenance : gives various infos for the upkeep
By default, the focus should be on Ordibus when hmi_light first opens.

__TODO: insert caption here__

Warnings are expected when executing hmi, an exemple is given below :
```bash
(hmi_light:4204): GLib-GObject-WARNING **: invalid cast from 'GtkTextView' to 'GtkMisc'
(hmi_light:4204): Gtk-CRITICAL **: gtk_misc_set_alignment: assertion 'GTK_IS_MISC (misc)' failed
(hmi_light:4204): Gtk-CRITICAL **: gtk_widget_set_events: assertion '!GTK_WIDGET_REALIZED (widget)' failed
```
### Dependencies

The build of this app depends on :
* Webkit-1.0 (see notes below)
* libsoup
* hmi_light sources from the hmi_light/src directory
* CommonLibs
* PaStates, Timer
* linked librairies : timers, Webkit, Gtk2, Alsa, rt, pthread

### Note

We can get rid of the Webkit dependency. It was only used in one particular case when connection to Tie was lost and hasn't been used in 5 years.


## SetDateTime

SetDateTime is an interface enabling you to change the date/time of your machine.

### How to run

1. You need to have display available on your machine.
2. Run `./SetDateTime` to launch the app and the window should open.

### Results

A window with the Date and Time of the system should be displayed with buttons to change those.

__TODO: insert caption here__

The following error can occur when changing the date/time :
`Error while setting manually entered date (errno 11 Resource temporarily unavailable)`
When changing unsuccessfully date/time, a pop-up saying "Date incorrecte" opens. Then, when you close that pop-up, the error above is printed in the terminal.

On Centos7, we get a warning saying
`Gtk-Message: 12:30:22.267: Failed to load module "canberra-gtk-module"`.
But, it doesn't prevent the application from working.

### Dependencies

The build of this app depends on :
* Gtk2
* CommonLibs
* linked librairies : Gtk2, TecLog, rt

### Note
SetDateTime appelé au début si heure pas automatique. Et si heure pas auto, une icone avec un calendrier permet sur Maintenance de remodifier l'heure si le chauffeur s'est trompé. Ce sont les seuls moments où SetDateTime est appelé.



On utilise RetrieveDate (GPS/réseau selon le premier qui répond) pour trouver la date. Si pas de réponse automatique, on demande au chauffeur de l'encoder via SetDateTime.
cf pb de pile

retrieve_date script appelle htpdate (appli) et utilise le gps_nmea_reader (appli qui lit des trames GPS pour récupérer la date et l'heure cf Dimitri). A priori, utilisé seulement dans ce cas.

AlerteOpera : envoyait des alertes, peut etre désactivé

AffInt : peut etre un faux ami

AffVideo : dossier afficheur video client, POC (test condition réelles 2-3 mois sur un bus)

DataTransfer : voir Antoine, envoie sgip ?

Diagnostic : lance des diagnostiques Kontron qui lancent des tests

GsmVoiceInhibition : utilisé pour envoyer un signal pour inhiber le GSM voice

hmi/spinner : le truc qui tourne sur l'image Tec au démarrage (gif qui tourne)
hmi/messagedisplay : pop up ? attention webkit

hmiPressKeySimulator : fonctionnel, permet de simuler l'appui de touches sur hmi

LogAndResetGsmData : adresses/ports GsmData mal configurés, nécessite un reset : voir Juan 

Ordibus : mix signal de porte, aff intérieurs, gsm voice, envoie des datas?... Tout ce qui est messagerie entre autre communication et appels, envoie position vers geoloc

OrdreDeMarche : réutilisation ancienne appli pour post MEC, encore utilisé. Récup ODM, parse, et stocke
OrdreDeMarcheVersion : voir Antoine, probablement inutilisé

ReadIVM : reconfigurer le bus la premiere fois, programme le numero du bus dans la memoire cablee

StoreEquiInfo : peut etre recup infos eqpts bus et les stocke (cf page web affichant les eqpts des bus ?)

watchdog : (de chez Kontron) a desactiver qd on fait certaines manip parce qu'il rebootait sinon 

WlanManager : gestion connection wifi, gere certificats des bus (cf certificats vu en ligne) et pleins d'autres trucs

Tests/TU/TU_PDS : fait en php, utilise hmipresskeysimulator, pour les touches pressées se référer à la map des touches de Dimitri, attention au modifications liées aux TECs.
Voir avec Antoine pour automatisation

calibtouch : recalibrer l'écran de la PFE ?

mq_emulator : fonctionnel voir Dimitri si pb

srwt_time : app pour modifier l'heure systeme, peut etre plus utilisé ?

#tail -f /var/log/messages pour avoir le déroulement de WriteLog sur la console
